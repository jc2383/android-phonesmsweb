package com.example.phonesmswebexample;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    final String TAG="PHONE-EXAMPLE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void callPhoneButtonPressed(View view) {

    }

    public void sendSMSButtonPressed(View view) {

    }
    public void openWebpageButtonPressed(View view) {

    }


}
